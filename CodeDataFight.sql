-- INSERTAR DATOS DE EJEMPLO

-- Insertar datos en la tabla WeightClass
INSERT INTO WeightClass (name) VALUES
  ('Flyweight'),
  ('Bantamweight'),
  ('Featherweight'),
  ('Lightweight'),
  ('Welterweight'),
  ('Middleweight'),
  ('Light Heavyweight'),
  ('Heavyweight');

-- Insertar datos en la tabla Team
INSERT INTO Team (name) VALUES
  ('Team Alpha'),
  ('Blackzilians'),
  ('American Top Team'),
  ('Jackson Wink MMA'),
  ('Tristar Gym');

-- Insertar datos en la tabla Fighter
INSERT INTO Fighter (name, nationality, team_id, weight_class_id) VALUES
  ('Conor McGregor', 'Ireland', 1, 4),
  ('Jon Jones', 'United States', 4, 7),
  ('Anderson Silva', 'Brazil', 5, 6),
  ('Khabib Nurmagomedov', 'Russia', 3, 4),
  ('Georges St-Pierre', 'Canada', 5, 7);

-- Insertar datos en la tabla Event
INSERT INTO Event (name, location, date) VALUES
  ('UFC 249', 'Las Vegas, NV', '2021-05-09'),
  ('Bellator 256', 'Uncasville, CT', '2021-04-09'),
  ('ONE Championship: Empower', 'Singapore', '2021-09-03');

-- Insertar datos en la tabla Method
INSERT INTO Method (name) VALUES
  ('Knockout'),
  ('Submission'),
  ('Decision');

-- Insertar datos en la tabla Fight
INSERT INTO Fight (event_id, fighter1_id, fighter2_id, winner_id, method_id) VALUES
  (1, 1, 2, 2, 1),
  (1, 3, 4, 3, 2),
  (2, 5, 2, 2, 3),
  (2, 3, 1, 1, 3),
  (3, 4, 5, 4, 1);

-- Insertar datos en la tabla Ranking
INSERT INTO Ranking (weight_class_id, fighter_id, wins, losses, knockouts, submissions) VALUES
  (4, 1, 22, 4, 19, 1),
  (7, 2, 26, 1, 10, 6),
  (6, 3, 34, 11, 21, 6),
  (4, 4, 29, 0, 8, 11),
  (7, 5, 26, 2, 8, 6);
