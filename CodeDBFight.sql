drop database fightMMA;
CREATE DATABASE fightMMA; 
USE fightMMA; 
-- Crear tabla Fighter
CREATE TABLE Fighter (
  fighter_id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  nationality VARCHAR(100) NOT NULL,
  team_id INT REFERENCES Team(team_id),
  weight_class_id INT REFERENCES WeightClass(weight_class_id)
  -- Otras columnas de detalles personales si es necesario
);

-- Crear tabla Event
CREATE TABLE Event (
  event_id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  location VARCHAR(255) NOT NULL,
  date DATE NOT NULL
  -- Otras columnas de detalles del evento si es necesario
);

-- Crear tabla Fight
CREATE TABLE Fight (
  fight_id SERIAL PRIMARY KEY,
  event_id INT REFERENCES Event(event_id),
  fighter1_id INT REFERENCES Fighter(fighter_id),
  fighter2_id INT REFERENCES Fighter(fighter_id),
  winner_id INT REFERENCES Fighter(fighter_id),
  method_id INT REFERENCES Method(method_id)
  -- Otras columnas de detalles de la pelea si es necesario
);

-- Crear tabla Ranking
CREATE TABLE Ranking (
  ranking_id SERIAL PRIMARY KEY,
  weight_class_id INT REFERENCES WeightClass(weight_class_id),
  fighter_id INT REFERENCES Fighter(fighter_id),
  wins INT NOT NULL,
  losses INT NOT NULL,
  knockouts INT NOT NULL,
  submissions INT NOT NULL
  -- Otras columnas de estadísticas si es necesario
);
-- Crear tabla WeightClass
CREATE TABLE WeightClass (
  weight_class_id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL
  -- Otras columnas de detalles de la clase de peso si es necesario
);
-- Crear tabla Team
CREATE TABLE Team (
  team_id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL
  -- Otras columnas de detalles de la clase de equipos si es necesario
);
-- Crear tabla Method
CREATE TABLE Method (
  method_id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL
  -- Otras columnas de detalles de la clase de metodos si es necesario
);
