# StarfundPT
## API de MMA Fight
Esta es una API de GraphQL para administrar datos relacionados con peleas de artes marciales mixtas (MMA).

## Instalación
Clona este repositorio:
https://gitlab.com/Maxwelljsm/starfundpt

## Instala las dependencias:
yarn install 

## Configura la base de datos:

Crea una base de datos en tu servidor de base de datos MySQL.
Configura las credenciales de acceso a la base de datos en el archivo database.js.
Ejecuta los scripts SQL provistos en el directorio para crear las tablas necesarias.

## Inicia el servidor
yarn start 

El servidor se ejecutará en http://localhost:5000.

## Uso

La API de MMA Fight proporciona los siguientes puntos finales:

/graphql: Punto final principal de GraphQL para interactuar con la API.
Asegúrate de tener una herramienta para interactuar con la API de GraphQL, como GraphiQL o Apollo Client. Puedes enviar consultas y mutaciones GraphQL a través de estas herramientas para interactuar con los datos.

## Ejemplos de consultas

### Consultar estaditicas de peleadores 

query {
  fighterStats(fighterId: "1") {
    fighter {
      fighterId
      name
      nationality
      team
      weightClassId
    }
    fightHistory {
      fightId
      eventId
      fighter1Id
      fighter2Id
      winnerId
      method
    }
  }
}

### Consultar estadisticas de eventos

query {
    upcomingEvents {
      eventId
      name
      location
      date
    }
  }

### Calcular y actualizar las clasificaciones de los luchadores después de cada pelea

mutation {
  updateRankings
}

### Consultar un evento por su ID:

query {
  getEvent(eventId: 1) {
    event_id
    name
    location
    date
  }
}
### Obtener todos los eventos:

query {
  getAllEvents {
    event_id
    name
    location
    date
  }
}
### Crear un nuevo evento:

mutation {
  createEvent(name: "Evento de prueba", location: "Ubicación de prueba", date: "2023-06-30") {
    event_id
    name
    location
    date
  }
}
### Actualizar un evento existente:

mutation {
  updateEvent(eventId: 1, name: "Nuevo nombre de evento") {
    event_id
    name
    location
    date
  }
}
### Eliminar un evento:

mutation {
  deleteEvent(eventId: 1) {
    event_id
    name
    location
    date
  }
}

NOTA: De igual manera para las mutaciones de luchadores y peleas


Aquí hay algunos ejemplos de consultas que puedes enviar a la API de MMA Fight:
