const Sequelize = require('sequelize')

var db = {}

const sequelize = new Sequelize('fightMMA', 'root', '2415', {
    host: '192.168.1.4',
    port: '3309',
    dialect: 'mysql',
    define: {
        freezeTableName: true,
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000,
    },
    // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
    operatorsAliases: false,
})

let models = [
    require('./models/Event.js'),
    require('./models/Fight.js'),
    require('./models/Fighter.js'),
    require('./models/Ranking.js'),
    require('./models/Team.js'),
    require('./models/Method.js'),
    require('./models/WeightClass.js'),
]

// Initialize models
models.forEach(model => {
    const seqModel = model(sequelize, Sequelize)
    db[seqModel.name] = seqModel
})

// Apply associations
Object.keys(db).forEach(key => {
    if ('associate' in db[key]) {
        db[key].associate(db)
    }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
