import { gql } from 'apollo-server-express'
import * as db from '../database'

export const typeDefs = gql`
    extend type Query {
        allFight: [Fight]
        fight(id: ID!): Fight
    }

    type Fight {
        fight_id: ID!
        event_id: Int
        event: Event
        fighter1_id: Int
        fight1: Fighter
        fighter2_id: Int
        fight2: Fighter
        winner_id: Int
        winner: Fighter
        method_id: Int
        method: Method
    }

    type MutationFight {
        createFight(fightInput: FightInput!): Fight
        updateFight(fightId: ID!, fightInput: FightInput!): Fight
        deleteFight(fightId: ID!): Fight
    }

    input FightInput {
        event_id: Int
        fighter1_id: Int
        fighter2_id: Int
        winner_id: Int
        method_id: Int
    }
`

export const resolvers = {
    Query: {
        fight: async (parent, { fightId }, { db }) => {
            try {
                // Buscar la pelea por su ID en la base de datos
                const fight = await db.Fight.findByPk(fightId)
                return fight
            } catch (error) {
                console.error('Error al obtener la pelea:', error)
                throw error
            }
        },
        allFight: async (parent, args, { db }) => {
            try {
                // Obtener todas las peleas de la base de datos
                const fights = await db.Fight.findAll()
                return fights
            } catch (error) {
                console.error('Error al obtener todas las peleas:', error)
                throw error
            }
        },
    },
    Fight: {
        event: async (obj, args, context, info) =>
            db.event.findByPk(obj.event_id),
        fight1: async (obj, args, context, info) =>
            db.fighter.findByPk(obj.fighter1_id),
        fight2: async (obj, args, context, info) =>
            db.fighter.findByPk(obj.fighter2_id),
        winner: async (obj, args, context, info) =>
            db.fighter.findByPk(obj.winner_id),
        method: async (obj, args, context, info) =>
            db.method.findByPk(obj.method_id),
    },
    MutationFight: {
        createFight: async (parent, { fightInput }, { db }) => {
            try {
                // Crear una nueva pelea en la base de datos
                const fight = await db.Fight.create(fightInput)
                return fight
            } catch (error) {
                console.error('Error al crear la pelea:', error)
                throw error
            }
        },
        updateFight: async (parent, { fightId, fightInput }, { db }) => {
            try {
                // Buscar la pelea por su ID en la base de datos
                const fight = await db.Fight.findByPk(fightId)
                if (!fight) {
                    throw new Error('Pelea no encontrada')
                }

                // Actualizar los datos de la pelea con los valores proporcionados
                Object.assign(fight, fightInput)

                // Guardar los cambios en la base de datos
                await fight.save()

                return fight
            } catch (error) {
                console.error('Error al actualizar la pelea:', error)
                throw error
            }
        },
        deleteFight: async (parent, { fightId }, { db }) => {
            try {
                // Buscar la pelea por su ID en la base de datos
                const fight = await db.Fight.findByPk(fightId)
                if (!fight) {
                    throw new Error('Pelea no encontrada')
                }

                // Eliminar la pelea de la base de datos
                await fight.destroy()

                return fight
            } catch (error) {
                console.error('Error al eliminar la pelea:', error)
                throw error
            }
        },
    },
}
