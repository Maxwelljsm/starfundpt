import { gql } from 'apollo-server-express'
import * as db from '../database'

export const typeDefs = gql`
    extend type Query {
        allRanking: [Ranking]
        ranking(id: ID!): Ranking
    }

    type Ranking {
        ranking_id: ID!
        weight_class_id: Int
        weightClass: WeightClass
        fighter_id: Int
        fighter: Fighter
        wins: String
        losses: String
        knockouts: String
        submissions: String
    }

    type Mutation {
        updateRankings: Boolean
      }
`

export const resolvers = {
    Query: {
        allRanking: async () => db.ranking.findAll(),
        ranking: async (obj, args, context, info) => db.ranking.findByPk(args.id),
    },
    Ranking: {
        fighter: async (obj, args, context, info) => db.fighter.findByPk(obj.fighter_id),
        weightClass: async (obj, args, context, info) => db.weightclass.findByPk(obj.weight_class_id),
    },
    Mutation: {
        updateRankings: async () => {
          try {
            // Lógica para calcular y actualizar las clasificaciones
            // 1. Obtén todas las peleas
            const fights = await db.fight.findAll();
    
            // 2. Recorre cada pelea y actualiza las clasificaciones de los luchadores
            for (const fight of fights) {
              const winnerId = fight.winner_id;
              const loserId = fight.fighter1_id === winnerId ? fight.fighter2_id : fight.fighter1_id;
    
              // 3. Obtén los registros de clasificación de los luchadores
              const winnerRanking = await db.ranking.findOne({ where: { fighter_id: winnerId } });
              const loserRanking = await db.ranking.findOne({ where: { fighter_id: loserId } });
    
              // 4. Actualiza las estadísticas de los luchadores
              winnerRanking.wins += 1;
              loserRanking.losses += 1;
    
              // 5. Guarda los cambios en la base de datos
              await winnerRanking.save();
              await loserRanking.save();
            }
    
            return true; // Indica que las clasificaciones se han actualizado correctamente
          } catch (error) {
            console.error('Error al calcular y actualizar las clasificaciones:', error);
            throw error;
          }
        }
      }
}
