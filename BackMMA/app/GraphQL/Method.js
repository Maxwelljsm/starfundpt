import { gql } from 'apollo-server-express'
import * as db from '../database'

export const typeDefs = gql`
    extend type Query {
        allMethod: [Method]
        method(id: ID!): Method
    }

    type Method {
        method_id: ID!
        name: String
    }
`

export const resolvers = {
    Query: {
        allMethod: async () => db.method.findAll(),
        method: async (obj, args, context, info) => 
        db.method.findByPk(args.id),
    },
}
