import { gql } from 'apollo-server-express'
import * as db from '../database'

export const typeDefs = gql`
    extend type Query {
        allWeightClass: [WeightClass]
        weightclass(id: ID!): WeightClass
    }

    type WeightClass {
        weight_class_id: ID!
        name: String
    }
`

export const resolvers = {
    Query: {
        allWeightClass: async () => db.weight_class.findAll(),
        weight_class: async (obj, args, context, info) => 
        db.weight_class.findByPk(args.id),
    },
}
