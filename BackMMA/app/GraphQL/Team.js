import { gql } from 'apollo-server-express'
import * as db from '../database'

export const typeDefs = gql`
    extend type Query {
        allTeam: [Team]
        team(id: ID!): Team
    }

    type Team {
        team_id: ID!
        name: String
    }
`

export const resolvers = {
    Query: {
        allTeam: async () => db.team.findAll(),
        team: async (obj, args, context, info) => 
        db.team.findByPk(args.id),
    },
}
