import { gql } from 'apollo-server-express'
import * as db from '../database'
const { Op } = require('sequelize')

export const typeDefs = gql`
    extend type Query {
        allFighter: [Fighter]
        fighter(id: ID!): Fighter
        fighterStats(fighterId: ID!): FighterStats
    }

    type FighterStats {
        fighter: Fighter
        fightHistory: [Fight]
    }

    type Fighter {
        fighter_id: ID!
        name: String
        nationality: String
        team_id: Int
        team: Team
        weight_class_id: Int
        weightClass: WeightClass
    }

    type MutationFighter {
        updateFighter(fighterId: ID!, updatedData: FighterInput!): Fighter
        createFighter(
            name: String!
            nationality: String!
            team_id: Int!
            weight_class_id: Int!
        ): Fighter
        deleteFighter(fighterId: ID!): Fighter
    }

    input FighterInput {
        name: String
        nationality: String
        team_id: Int
        weight_class_id: Int
    }
`

export const resolvers = {
    Query: {
        fighterStats: async (parent, args) => {
            // Lógica para recuperar las estadísticas de peleas de un peleador específico
            // Utiliza el argumento "fighterId" para buscar el peleador en la base de datos y obtener sus estadísticas de peleas
            const { fighterId } = args

            try {
                // Obtener los datos del peleador por su ID
                const fighter = await db.fighter.findByPk(fighterId)
                if (!fighter) {
                    throw new Error('Peleador no encontrado')
                }

                // Obtener el historial de peleas del peleador
                const fightHistory = await db.fight.findAll({
                    where: {
                        [Op.or]: [
                            { fighter1_id: fighterId },
                            { fighter2_id: fighterId },
                        ],
                    },
                })

                // Retornar el objeto con las estadísticas del peleador y su historial de peleas
                return {
                    fighter,
                    fightHistory,
                }
            } catch (error) {
                console.error(
                    'Error al obtener las estadísticas del peleador:',
                    error,
                )
                throw error
            }
        },
        allFighter: async () => db.fighter.findAll(),
        fighter: async (obj, args, context, info) =>
            db.fighter.findByPk(args.id),
    },
    Fighter: {
        team: async (obj, args, context, info) => db.team.findByPk(obj.team_id),
        weightClass: async (obj, args, context, info) =>
            db.weightclass.findByPk(obj.weight_class_id),
    },
    MutationFighter: {
        // Mutación para crear un nuevo luchador
        createFighter: async (
            parent,
            { name, nationality, team_id, weight_class_id },
        ) => {
            // Lógica para crear un nuevo luchador en la base de datos
            try {
                const newFighter = await db.fighter.create({
                    name,
                    nationality,
                    team_id,
                    weight_class_id,
                })
                return newFighter
            } catch (error) {
                console.error('Error al crear el luchador:', error)
                throw error
            }
        },

        // Mutación para actualizar un luchador existente
        updateFighter: async (parent, { fighterId, updatedData }) => {
            // Lógica para actualizar un luchador en la base de datos
            try {
                // Busca el luchador en la base de datos por su ID
                const fighter = await db.fighter.findByPk(fighterId)
                if (!fighter) {
                    throw new Error('Luchador no encontrado')
                }
                // Actualiza los datos del luchador con los valores proporcionados
                Object.assign(fighter, updatedData)
                // Guarda los cambios en la base de datos
                const updatedFighter = await fighter.save()
                return updatedFighter
            } catch (error) {
                // Manejo de errores
                console.error('Error al actualizar el luchador:', error)
                throw error
            }
        },

        // Mutación para eliminar un luchador
        deleteFighter: async (parent, { fighterId }) => {
            // Lógica para eliminar un luchador de la base de datos
            try {
                // Buscar el luchador en la base de datos
                const fighter = await db.Fighter.findByPk(fighterId)
                if (!fighter) {
                    throw new Error('Luchador no encontrado')
                }
                // Eliminar el luchador de la base de datos
                await fighter.destroy()
                return fighter
            } catch (error) {
                console.error('Error al eliminar el luchador:', error)
                throw error
            }
        },
    },
}
