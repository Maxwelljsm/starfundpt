import { gql } from 'apollo-server-express'
import * as db from '../database'
const { Op } = require('sequelize')

export const typeDefs = gql`
    extend type Query {
        allEvent: [Event]
        event(id: ID!): Event
        upcomingEvents: [Event]
    }

    type Event {
        event_id: ID!
        name: String
        location: String
        date: String
    }

    type MutationEvent {
        updateEvent(eventId: ID!, updatedData: EventInput!): Event
        createEvent(
            name: String!
            location: String!
            date: Int!
            weight_class_id: Int!
        ): Event
        deleteEvent(eventId: ID!): Event
    }

    input EventInput {
        name: String
        nationality: String
        team_id: Int
        weight_class_id: Int
    }
`

export const resolvers = {
    Query: {
        upcomingEvents: async () => {
            // Lógica para recuperar los próximos eventos
            try {
                // Obtiene la fecha actual
                const currentDate = new Date()

                // Realiza la consulta a la base de datos para obtener los eventos cuya fecha sea posterior a la fecha actual
                const events = await db.event.findAll({
                    where: {
                        date: {
                            [Op.gte]: currentDate, // Utiliza el operador "greater than or equal to" para comparar las fechas
                        },
                    },
                })

                // Retorna un arreglo de objetos que representan los próximos eventos
                return events
            } catch (error) {
                console.error('Error al obtener los próximos eventos:', error)
                throw error
            }
        },
        event: async (parent, { eventId }, { db }) => {
            try {
                // Buscar el evento por su ID en la base de datos
                const event = await db.Event.findByPk(eventId)
                return event
            } catch (error) {
                console.error('Error al obtener el evento:', error)
                throw error
            }
        },
        allEvent: async (parent, args, { db }) => {
            try {
                // Obtener todos los eventos de la base de datos
                const events = await db.Event.findAll()
                return events
            } catch (error) {
                console.error('Error al obtener todos los eventos:', error)
                throw error
            }
        },
    },
    MutationEvent: {
        createEvent: async (parent, { name, location, date }, { db }) => {
            try {
                // Crear un nuevo evento en la base de datos
                const event = await db.Event.create({
                    name,
                    location,
                    date,
                })
                return event
            } catch (error) {
                console.error('Error al crear el evento:', error)
                throw error
            }
        },
        updateEvent: async (
            parent,
            { eventId, name, location, date },
            { db },
        ) => {
            try {
                // Buscar el evento por su ID en la base de datos
                const event = await db.Event.findByPk(eventId)
                if (!event) {
                    throw new Error('Evento no encontrado')
                }

                // Actualizar los datos del evento con los valores proporcionados
                event.name = name || event.name
                event.location = location || event.location
                event.date = date || event.date

                // Guardar los cambios en la base de datos
                await event.save()

                return event
            } catch (error) {
                console.error('Error al actualizar el evento:', error)
                throw error
            }
        },
        deleteEvent: async (parent, { eventId }, { db }) => {
            try {
                // Buscar el evento por su ID en la base de datos
                const event = await db.Event.findByPk(eventId)
                if (!event) {
                    throw new Error('Evento no encontrado')
                }

                // Eliminar el evento de la base de datos
                await event.destroy()

                return event
            } catch (error) {
                console.error('Error al eliminar el evento:', error)
                throw error
            }
        },
    },
}
