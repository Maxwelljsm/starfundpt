/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('event', {
		event_id: {
			type: DataTypes.INTEGER.UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(64),
			allowNull: false
		},
		location: {
			type: DataTypes.STRING(256),
			allowNull: false
		},
		date: {
			type: DataTypes.DATE(),
			allowNull: false
		}
	}, {
		tableName: 'event',
		timestamps: false
	});
};
