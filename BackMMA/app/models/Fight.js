/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('fight', {
		fight_id: {
			type: DataTypes.INTEGER.UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		event_id: {
			type: DataTypes.INTEGER ,
			allowNull: false
		},
		fighter1_id: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		fighter2_id: {
			type: DataTypes.INTEGER ,
			allowNull: false
		},
		winner_id: {
			type: DataTypes.INTEGER ,
			allowNull: false
		},
		method_id: {
			type: DataTypes.INTEGER ,
			allowNull: false
		}
	}, {
		tableName: 'fight',
		timestamps: false
	});
};
