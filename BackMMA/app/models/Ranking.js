/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ranking', {
		ranking_id: {
			type: DataTypes.INTEGER.UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		weight_class_id: {
			type: DataTypes.INTEGER ,
			allowNull: false
		},
		fighter_id: {
			type: DataTypes.INTEGER ,
			allowNull: false
		},
		wins: {
			type: DataTypes.STRING(256),
			allowNull: false
		},
		losses: {
			type: DataTypes.STRING(256),
			allowNull: false
		},
		knockouts: {
			type: DataTypes.STRING(256),
			allowNull: false
		},
		submissions: {
			type: DataTypes.STRING(256),
			allowNull: false
		}
	}, {
		tableName: 'ranking',
		timestamps: false
	});
};
