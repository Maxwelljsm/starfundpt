/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('method', {
		method_id: {
			type: DataTypes.INTEGER.UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(256),
			allowNull: false
		}
	}, {
		tableName: 'method',
		timestamps: false
	});
};
