/* jshint indent: 1 */
module.exports = function(sequelize, DataTypes) {

	return sequelize.define('fighter', {
		fighter_id: {
			type: DataTypes.INTEGER .UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(256),
			allowNull: false
		},
		nationality: {
			type: DataTypes.STRING(256),
			allowNull: false
		},
		team_id: {
			type: DataTypes.INTEGER ,
			allowNull: false
		},
		weight_class_id: {
			type: DataTypes.INTEGER ,
			allowNull: false
		}
	}, {
		tableName: 'fighter',
		timestamps: false
	});
};
