/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('weightclass', {
		weight_class_id: {
			type: DataTypes.INTEGER.UNSIGNED,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(256),
			allowNull: false
		}
	}, {
		tableName: 'weightclass',
		timestamps: false
	});
};
