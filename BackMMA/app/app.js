import '@babel/polyfill'
import express from 'express'
const bodyParser = require('body-parser')
const { ApolloServer } = require('apollo-server-express')
const cors = require('cors')
const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

const server = new ApolloServer({
    modules: [
        require('./GraphQL/Event'),
        require('./GraphQL/Fight'),
        require('./GraphQL/Fighter'),
        require('./GraphQL/Method'),
        require('./GraphQL/Team'),
        require('./GraphQL/WeightClass'),
        require('./GraphQL/Ranking'),
    ],
})

server.applyMiddleware({ app })

app.get('/', (req, res) => res.send('Hello World!'))

app.listen({ port: 5000 }, () =>
    console.log(`🚀 Server ready at http://192.168.1.4:5000`),
)
